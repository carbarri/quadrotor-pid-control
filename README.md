# Quadrotor PID control

MatLab and SimuLink codes for the desing of a PID control and state observer of a quadrotor helicopter.

[Here](https://carbarri.gitlab.io/quadrotor-pid-control/) you can see the summarized report.

[Here](https://gitlab.com/carbarri/quadrotor-pid-control/-/blob/main/PracticaQuadCMBG.pdf) you can see the full report. 



