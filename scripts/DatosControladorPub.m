%% *Control de un helicóptero de cuatro hélices con tres grados de libertad.* 
%%
%El siguiente script realiza un control PID con observador de estados.

ModeloQuanser;               % Parámetros del modelo.

t = linspace(0,20,2000);    % Tiempo de simulación.

%% Señales de referencia.
% Se definen las señales de referencia para la acción directa. Podemos 
% escoger una señal cuadrada para alabeo, cabeceo o guiñada poniendo un 0 
% en el vector switx, con frecuencias reffreq y amplitudes refamp. Además 
% podemos elegir si queremos incluir el observador en el control poniendo a
% 0 la variable est (con estimador).
%
% Se han escogido las mismas frecuencias porque es una situación extrema
% para el sistema, dado que tiene que moverse en los tres ejes a la vez y
% para ello ha de consumir mucha energia

fir = pi/12;        % alabeo en radianes
ther = pi/12;       % cabeceo en radianes
psir = pi/2;        % guiñada en radianes

switx = [0,0,0] ;
refamp = [fir,ther,psir];
reffreq = [0.1,0.1,0.1];

% Perturbaciones.
PertOutAmp = 1;
PertInAmp = [0.2,0,0.2,0];

%% Parámetros de control.
% Empezamos definiendo las matrices del sistema para alabeo y cabeceo y
% para la guiñada.

% CA
Aca = [0,1,0;0,0,d*Kf/Ir;0,0,-1/Taup];
Bca = [0,0,1/Taup]';
Cca = [1,0,0];    
Dca = 0;

% G
Ag = [0,1,0;0,0,Kt/Iy;0,0,-1/Taup];
Bg = [0,0,1/Taup]';
Cg = [1,0,0];
Dg = 0;

%% Acción integral.
% Definimos las matrices ampliadas para introducir acción integral.

% CA 
AcaAi =[Aca,zeros(size(Aca,1),1);Cca,0];
BcaAi =[Bca;0];
CcaAi =[Cca,0];

% G
AgAi =[Ag,zeros(size(Ag,1),1);Cg,0];
BgAi =[Bg;0];
CgAi =[Cg,0];

%% Control lineal cuadrático.
% Elegimos valores para los márgenes de error del control de las variables
% del sistema.
%
% Los valores han sido escogidos por prueba y error y más tare se ha
% realizado un bucle en torno a estos valores para confirmar que han sido
% bien escogidos. 
% El peso de la acción integral no ha sido determinante hasta que se han 
% incluido perturbaciones en la entrada. Debido a ello, el overshoot ha 
% aumentado. Para compensarlo, se ha reducido la ganancia de acción directa
% a la mitad de la ganancia proporcional. Para la guiñada no se hareducido 
% la accion integral dado que es muy difícil que una perturbacion de 
% entrada la afecte.
% Además, al incluir pertubaciones en la salida fue necesario dar más peso 
% al término derivativo. 
% La constante del motor se ha mantenido con un peso bajo dado que en este 
% caso concreto no se estudian variaciones en la altura.
% Finalmente el peso en la señal de control se ha usado para ajustar los
% voltajes por debajo del límite de saturación que se ha tomado en 7+5
% voltios.
%
% Se adjunta un video en el que se ven los detalles.


AngcaMax = 0.12;
VelcaMax = 0.15;
XmcaMax = 10;
KicaMax = 0.12;
VcaMax = 0.5;

AnggMax = 0.04; 
VelgMax = 0.06;
XmgMax = 10;
KigMax = 30;
VgMax = 0.22

%% Matrices Q y R.
QcaAi=[1/AngcaMax^2,0,0,0;0,1/VelcaMax^2,0,0;0,0,1/XmcaMax^2,0;0,0,0,1/KicaMax^2];
RcaAi=(1/VcaMax)^2;

QgAi=[1/AnggMax^2,0,0,0;0,1/VelgMax^2,0,0;0,0,1/XmgMax^2,0;0,0,0,(1/KigMax)^2];
RgAi=(1/VgMax)^2;

QcaAi
RcaAi
QgAi
RgAi

%% Ganancias y autovalores.
[KcaAi,Sbrp,Ebrp] = lqr(AcaAi,BcaAi,QcaAi,RcaAi);
Fca  = KcaAi(1)/2;
[KgAi,Sby,Eby] = lqr(AgAi,BgAi,QgAi,RgAi);
Fg  = KgAi(1);

% Ganancias del observador.
Lca = acker(Aca',Cca',Ebrp(1:3)*5)';
Lg = acker(Ag',Cg',Eby(1:3)*5)';

KcaAi
KgAi
%% Gráficas.
% Rechazo a perturbaciones.
% Se introdujeron dos perturbaciones, una en la salida de tipo delta (en los 
% ángulos) y otra en la entrada de tipo escalón (sobre la señal de control) y 
% otra, en ambos casos se observa como el control las rechaza. En las
% gráficas se respectivamente la primera y la segunda. En el video se puede
% observar como responde el sistema para distintas ganancias.
% Intuitivamente podemos entender estas perturbaciones como un golpe, la
% primera, que cambia repentinamente los ángulo y como viento, la segunda,
% a la que los motores responden para mantener la posición de equilibrio.

sim('ModeloNoLinealAlumnos');
%%
%
figure(1);plot(tiempo,(360/(2*pi))*out(:,2),tiempo,(360/(2*pi))*out(:,5),'-',tiempo,(360/(2*pi))*ae(:,1),'--',tiempo,(360/(2*pi))*PertIn,'-.k',tiempo,(360/(2*pi))*PertOut,'-.r');
title('Alabeo');legend('Referencia','Salida','Estimada','Perturbación entrada','Perturbacion salida');ylabel('Ángulo(°)');xlabel('Tiempo(s)');
%%
%
figure(2);plot(tiempo,(360/(2*pi))*out(:,3),tiempo,(360/(2*pi))*out(:,6),'-',tiempo,(360/(2*pi))*ce(:,1),'--',tiempo,PertIn);
title('Cabeceo');ylabel('Ángulo(°)');xlabel('Tiempo(s)');
%%
%
figure(3);plot(tiempo,(360/(2*pi))*out(:,4),tiempo,(360/(2*pi))*out(:,7),'-',tiempo,(360/(2*pi))*ge(:,1),'--');
title('Guiñada');xlabel('Tiempo(s)');ylabel('Ángulo(°)');
%%
%
figure(4);plot(tiempo,Vfbrlout,tiempo,12*ones(1,length(tiempo)),'--k')
legend('proa','popa','estribor','babor');xlabel('Tiempo(s)');ylabel('Voltaje(V)');title('Control motores');
%%
%
figure(5);plot(tiempo,Vsat)
legend('proa','popa','estribor','babor');xlabel('Tiempo(s)');ylabel('Voltaje(V)');title('Despues del saturador');
%% Margen de estabilidad.
% Calculamos los márgenes de estabilidad para cada caso.
% Matrices del controlador. Dos entradas.
Acca = [Aca-Bca*KcaAi(1:3)-Lca*Cca,-Bca*KcaAi(4);zeros(1,3),0];
Bcca = [Lca,Bca*Fca;1,-1];
Ccca = KcaAi;
Dcca = [0,Fca];

Acg = [Ag-Bg*KgAi(1:3)-Lg*Cg,-Bg*KgAi(4);zeros(1,3),0];
Bcg = [Lg,Bg*Fg;1,-1];
Ccg = KgAi;
Dcg = [0,Fg];
%%
% Funciones de transferencia. 
% Del controlador, con dos entradas (señal de salida y referencia), y de
% la planta.
%%
% *Para cabeceo y alabeo.*
Gcrca = tf(ss(Acca,Bcca,Ccca,Dcca));   

%%
% *Planta* 
Gca                             
Gca = tf(ss(Aca,Bca,Cca,Dca));         

%%
% *U(s)/Y(s)* 
Gcca = Gcrca(1);                      
Gcca
%%
% *U(s)/R(s)* 
Grca = Gcrca(2);
Grca

%%
% *Para la guiñada.*

Gcrg = tf(ss(Acg,Bcg,Ccg,Dcg));  

%%
% *Planta*                    
Gg = tf(ss(Ag,Bg,Cg,Dg));    
Gg

%%
% *U(s)/Y(s)*
Gcg = Gcrg(1);                  
Gcg

%%
% *U(s)/R(s)*
Grg = Gcrg(2);
Grg

%% Márgenes de estabilidad y diagrama de Bode.
% Menor distancia al punto crítico en el diagrama de Nyquist (-1).
%%
% *Cabeceo y alabeo:*
figure(6);
margin(Gca*Gcca);
[Wca,Minca]=fminunc(@(w)abs(1+freqresp(Gcca*Gca,w)),1);
Minca
%%
% *Guiñada:*
figure(7);
margin(Gg*Gcg);
[Wg,Ming]=fminunc(@(w)abs(1+freqresp(Gcg*Gg,w)),1);
Ming


