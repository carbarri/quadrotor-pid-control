%% ModeloQuanser.m
%
% Contiene los parametros del ModeloQuanser.mdl
% Este archivo se lee al cargar el modelo Simulink

M = 2.85; % Kg
g = 9.8;  % m/s^2
d = 0.1969; % m
Ir = 0.0552; % Kg m^2
Ip = 0.0552; % Kg m^2
Iy = 0.1104; % Kg m^2
Kf = 0.5940; % N/Volt
Kt = 0.0108; % Nm / Volt
Taup = 0.15; % seg


% Valores iniciales
x0p = [0,0,0]';  % Posicion inicial geogr�fica m
x0v = [0,0,0]';  % Velocidad inicial ejes cuerpo m/s
x0ang = [0,0,0]'; % Angulos iniciales radianes [phi=roll,the=pitch,psi=yaw]
x0w = [0,0,0]'; % Velocidad angular rad/seg
x0m = M*g/4;  % Posici�n inicial de los motores para compensar g

% Matriz de transformacion de controles para ejes NED
% u = [uz, ur, up, uy]';
% V = [Vf, Vb, Vr, Vl]';
Ku2V = [    1/4,    0,  1/2,  1/4; 
            1/4,    0, -1/2,  1/4;
            1/4, -1/2,    0, -1/4;
            1/4,  1/2,    0, -1/4   ];
KV2u = inv(Ku2V);
% KV2u =[ 1     1     1     1
%         0     0     1    -1
%         1    -1     0     0
%         1     1    -1    -1];
% Ku2V=inv(KV2u);    

