%% DatosControl
% Fichero para ejecutar los modelos Simulink

ModeloQuanser   % Par�metros del modelo

t = linspace(0,60,2000);

%% Referencias.
switx = [0,0,0] % switx(1,2 ó 3) = 0 ⨪> ref onda cuadrada en fi, the o psi. 
est = 1;        % 1 = sin estimador 

fir = pi/12;        % radianes
ther = pi/12;       % radianes
psir = pi/2;  % radianes
refamp = [fir,ther,psir];
reffreq = [0.1,0.1,0.1]

% Perturbaciones.
PertOutAmp = 1;
PertInAmp = [0.2,0,0.2,0];

%% Parametros de control.

% CABECEO Y ALABEO
%[AGca,BGca,CGca,DGca] = tf2ss(14.13,[1,6.667,0,0])

Aca = [0,1,0;0,0,d*Kf/Ir;0,0,-1/Taup]
Bca = [0,0,1/Taup]'
Cca = [1,0,0]    
Dca = 0;
AVaDca = [-2,-2,-2];
Kca = acker(Aca,Bca,AVaDca);
Fca = Kca(1);
Lca = acker(Aca',Cca',AVaDca*5)';

% GUIÑADA
%[AGg,BGg,CGg,DGg] = tf2ss(0.6522,[1,6.667,0,0]);

Ag = [0,1,0;0,0,Kt/Iy;0,0,-1/Taup]
Bg = [0,0,1/Taup]'
Cg = [1,0,0]
Dg = 0;

AVaDg = [-1,-1,-1];
Kg = acker(Ag,Bg,AVaDg);
Fg = Kg(1);
Lg = acker(Ag',Cg',AVaDg*20)';

%% Accion integral

% CA 
AcaAi =[Aca,zeros(size(Aca,1),1);Cca,0]
BcaAi =[Bca;0]
CcaAi =[Cca,0]

AvaDcaAi = [-2,-2,-2,-2]'
KcaAi = acker(AcaAi,BcaAi,AvaDcaAi)
Lca = acker(Aca',Cca',AvaDcaAi(1:3)*5)';

% G
AgAi =[Ag,zeros(size(Ag,1),1);Cg,0]
BgAi =[Bg;0]
CgAi =[Cg,0]

AvaDgAi = [-2,-2,-2,-2]'
KgAi = acker(AgAi,BgAi,AvaDgAi)
Lg = acker(Ag',Cg',AvaDgAi(1:3)*5)';
%% LQR
% Los valores han sido escogidos por prueba error y más tare se ha
% realizado un bucle en torno a estos valores para confirmar que han sido
% bien escogidos. El peso de la acción integral no ha sido determinante en 
% hasta que se han incluido perturbaciones en la entrada. Debido a ello, el
% overshoot ha aumentado. Para compensarlo, se ha reducido la ganancia de
% acción directa a la mitad de la ganancia proporcional. Para la guiñada no
% se hareducido la accion integral dado que es muy difícil que una
% perturbacion de entrada la afecte.
% Además, al incluir pertubaciones en la salida fue necesario dar más peso 
% al término derivativo. 
% La constante del motor se ha mantenido con un peso bajo dado que en este 
% caso concreto no se estudian variaciones en la altura.
% Finalmente el peso en la señal de control se ha usado para ajustar los
% voltajes por debajo del límite de saturación que se ha tomado en 7+5
% voltios.

% CA
clear KcaAi Ebrp AngcaMax VelcaMax XmcaMax KicaMax VcaMax
AngcaMax = 0.32;
VelcaMax = 0.15;
XmcaMax = 10;
KicaMax = 0.2;
VcaMax = 0.2;

QcaAi=[1/AngcaMax^2,0,0,0;0,1/VelcaMax^2,0,0;0,0,1/XmcaMax^2,0;0,0,0,(1/KicaMax)^2];
%[1/0.1^2,0,0,0;0,1/5^2,0,0;0,0,1/7^2,0;0,0,0,5]
RcaAi=(1/VcaMax)^2;
%1/0.1^2


[KcaAi,Sbrp,Ebrp] = lqr(AcaAi,BcaAi,QcaAi,RcaAi);

Fca  = KcaAi(1)/2;
Lca = acker(Aca',Cca',Ebrp(1:3)*5)';

% G
clear KgAi Eby AnggMax VelgMax XmgMax KigMax VgMax
AnggMax = 0.01; 
VelgMax = 0.1;
XmgMax = 1;
KigMax = 0.1;
VgMax = 0.006;

QgAi=[1/AnggMax^2,0,0,0;0,1/VelgMax^2,0,0;0,0,1/XmgMax^2,0;0,0,0,(1/KigMax)^2];
%[1/0.2^2,0,0,0;0,1/0.5^2,0,0;0,0,1/7^2,0;0,0,0,5]

RgAi=(1/VgMax)^2;
%1/0.5^2

[KgAi,Sby,Eby] = lqr(AgAi,BgAi,QgAi,RgAi);
Fg  = KgAi(1)/2;
Lg = acker(Ag',Cg',Eby(1:3)*5)';

% CA - Margen de estabilidad

% Matrices del controlador. Dos entradas.
Acca = [Aca-Bca*KcaAi(1:3)-Lca*Cca,-Bca*KcaAi(4);zeros(1,3),0];
Bcca = [Lca,Bca*Fca;1,-1];
Ccca = KcaAi;
Dcca = [0,Fca];

Gcrca = tf(ss(Acca,Bcca,Ccca,Dcca));  % Funcion de transferencia del controlador.
Gcca = Gcrca(1);                  % F.t. de U(s)/Y(s).
Grca = Gcrca(2);
Gca = tf(ss(Aca,Bca,Cca,Dca));       % F.t. de la planta.

[MGca,MFca,WGca,WFca] = margin(Gca*Gcca);
MGcadB = 20*log10(MGca)
MFca
%figure(5)
%margin(Gca*Gcca)
% G - Margen de estabilidad

% Matrices del controlador. Dos entradas.
Acg = [Ag-Bg*KgAi(1:3)-Lg*Cg,-Bg*KgAi(4);zeros(1,3),0];
Bcg = [Lg,Bg*Fg;1,-1];
Ccg = KgAi;
Dcg = [0,Fg];

Gcrg = tf(ss(Acg,Bcg,Ccg,Dcg));  % Funcion de transferencia del controlador.
Gcg = Gcrg(1);                  % F.t. de U(s)/Y(s).
Grg = Gcrg(2);                  
Gg = tf(ss(Ag,Bg,Cg,Dg));        % F.t. de la planta.

[MGg,MFg,WGg,WFg] = margin(Gg*Gcg);
MGgdB = 20*log10(MGg)
MFg

%[Wca,Minca]=fminunc(@(w)abs(1+freqresp(Gcca*Gca,w)),1);
%[Wg,Ming]=fminunc(@(w)abs(1+freqresp(Gcg*Gg,w)),1);

% Calculo analitico.
%syms wg wf
%Wg = solve(phase(subs(tf2sym(Gcg)*tf2sym(Gg),wg*1i))-pi,wg)
%Wf = solve(norm(subs(tf2sym(Gcg)*tf2sym(Gg),wf*1i))-1,wf)
%MG = 1/(norm(subs(tf2sym(Gcg)*tf2sym(Gg),Wg*1i)))
%MF = pi + phase(subs(tf2sym(Gcg)*tf2sym(Gg),wf*1i))
%EstLim = solve(tf2sym(Gcg)*tf2sym(Gg)+1)


% Plotear
sim('ModeloNoLinealAlumnos');
figure(3)
subplot(311);plot(tiempo,out(:,2),tiempo,out(:,5),'-',tiempo,ae(:,1),'--');
subplot(312);plot(tiempo,out(:,3),tiempo,out(:,6),'-',tiempo,ce(:,1),'--');
subplot(313);plot(tiempo,out(:,4),tiempo,out(:,7),'-',tiempo,ge(:,1),'--');
figure(4)
subplot(211);plot(tiempo,Vfbrlout,tiempo,12*ones(1,length(tiempo)),'--k')
legend('f','b','r','l')
subplot(212);plot(tiempo,Vsat)
legend('f','b','r','l')
%hold on
%% Optimizacion QR
% CA
clear KcaAi Ebrp AngcaMax VelcaMax XmcaMax KicaMax VcaMax
clear KgAi Eby AnggMax VelgMax XmgMax KigMax VgMax QR Qrnum QRT
clear outaT outcT outgT outvT
%%
AngcaMax = 0.12;
VelcaMax = 0.15;
XmcaMax = 10;
KicaMax = 0.12;
VcaMax = 0.5;

AnggMax = 0.04; 
VelgMax = 0.06;
XmgMax = 10;
KigMax = 30;
VgMax = 0.22;

Nloop = 50;
%[AngcaMax,VelcaMax,XmcaMax,KicaMax,VcaMax;AnggMax,VelgMax,XmgMax,KigMax,VgMax];
QRnum = [0.12,0.15,10,0.12,0.5;0.04,0.06,10,30,0.22];
QR = [1,1,1,1,1;1,1,1,1,1];
for k=1:2;
for j=1:5;
for i=1:Nloop;

QR(k,j) = QRnum(k,j)^((i-1)/49);
QRT(k,j,i) = QR(k,j);

QcaAi=[1/QR(1,1)^2,0,0,0;0,1/QR(1,2)^2,0,0;0,0,1/QR(1,3)^2,0;0,0,0,1/QR(1,4)^2];
RcaAi=(1/QR(1,5))^2;

[KcaAi,Sbrp,Ebrp] = lqr(AcaAi,BcaAi,QcaAi,RcaAi);
KcaAiT(k,j,i,:)=KcaAi;
EbrpT(k,j,i,:)=Ebrp;

Fca = KcaAi(1)/2;
Lca = acker(Aca',Cca',Ebrp(1:3)*5)';

% G

QgAi=[1/QR(2,1)^2,0,0,0;0,1/QR(2,2)^2,0,0;0,0,1/QR(2,3)^2,0;0,0,0,1/QR(2,4)^2];
RgAi=(1/QR(2,5))^2;

[KgAi,Sby,Eby] = lqr(AgAi,BgAi,QgAi,RgAi);

KgAiT(k,j,i,:)=KgAi;
EbyT(k,j,i,:)=Eby;

Fg  = KgAi(1);
Lg = acker(Ag',Cg',Eby(1:3)*5)';
sim('ModeloNoLinealAlumnos',t);
outaT(k,j,i,:) = out(:,5);
outcT(k,j,i,:) = out(:,6);
outgT(k,j,i,:) = out(:,7);
outvT(k,j,i,:,:) = Vfbrlout;
% CA - Margen de estabilidad
end
end
end
%%
% Matrices del controlador. Dos entradas.
Acca = [Aca-Bca*KcaAi(1:3)-Lca*Cca,-Bca*KcaAi(4);zeros(1,3),0];
Bcca = [Lca,Bca*Fca;1,-1];
Ccca = KcaAi;
Dcca = [0,Fca];

Gcrca = tf(ss(Acca,Bcca,Ccca,Dcca));  % Funcion de transferencia del controlador.
Gcca = Gcrca(1);                  % F.t. de U(s)/Y(s).
%Grca = Gcrca(2);
Gca = tf(ss(Aca,Bca,Cca,Dca));       % F.t. de la planta.

[MGca(k,j,i),MFca(k,j,i),WGca,WFca] = margin(Gca*Gcca);
%MGcadB = 20*log10(MGca)
%MFca
%figure(5)
%margin(Gca*Gcca)
% G - Margen de estabilidad

% Matrices del controlador. Dos entradas.
Acg = [Ag-Bg*KgAi(1:3)-Lg*Cg,-Bg*KgAi(4);zeros(1,3),0];
Bcg = [Lg,Bg*Fg;1,-1];
Ccg = KgAi;
Dcg = [0,Fg];

Gcrg = tf(ss(Acg,Bcg,Ccg,Dcg));  % Funcion de transferencia del controlador.
Gcg = Gcrg(1);                  % F.t. de U(s)/Y(s).
%Grg = Gcrg(2);                  
Gg = tf(ss(Ag,Bg,Cg,Dg));        % F.t. de la planta.

[MGg(k,j,i),MFg(k,j,i),WGg,WFg] = margin(Gg*Gcg);
%MGgdB = 20*log10(MGg)
%MFg

[Wca(k,j,i),Minca(k,j,i)]=fminunc(@(w)abs(1+freqresp(Gcca*Gca,w)),1);
[Wg(k,j,i),Ming(k,j,i)]=fminunc(@(w)abs(1+freqresp(Gcg*Gg,w)),1);

k
j
i


end
end
end

%%


plot(permute(QRT(1,1,:),[3,2,1]),permute(Minca(1,1,:),[3,2,1]),'o')
figure(1)

subplot(421);plot(AnggMax,KgAi(1,:))
ylabel('Kp_y');title('Ganancias');
subplot(422);plot(AnggMax,KgAi(2,:))
ylabel('Kd_y');title('Ganancias');
subplot(423);plot(AnggMax,KgAi(3,:))
xlabel('Max');ylabel('Km_y');
subplot(424);plot(AnggMax,KgAi(4,:))
xlabel('Max');ylabel('Ki_y');

subplot(425);plot(AnggMax,real(Eby(1,:)),AnggMax,imag(Eby(1,:)))
title('Autovalores');ylabel('Eby_11');legend('Re','Im')
subplot(426);plot(AnggMax,real(Eby(2,:)),AnggMax,imag(Eby(2,:)))
title('Autovalores');ylabel('Eby_22');
subplot(427);plot(AnggMax,real(Eby(3,:)),AnggMax,imag(Eby(3,:)))
xlabel('Max');ylabel('Eby_33');
subplot(428);plot(AnggMax,real(Eby(4,:)),AnggMax,imag(Eby(4,:)))
xlabel('Max');ylabel('Eby_44');




%% Animacion

Nloop=50
clear Xtot Ttot Xreftot

for i=1:Nloop
    
    QcaAi=(i/5)*((8/pi)^2)*eye(4);
    RcaAi=(i/5)*(8/pi)^2;
    [KcaAi,Sbrp,Ebrp] = lqr(AcaAi,BcaAi,QcaAi,RcaAi)
    
    %AvaDcaAi = [-2,-2,-2,-2]';
    %KcaAi = acker(AcaAi,BcaAi,AvaDcaAi);
    Lca = acker(Aca',Cca',AvaDcaAi(1:3)*20)';
    
    QgAi=(i/5)*(1/pi^2)*eye(4);
    RgAi=(i/5)*(1/pi)^2;
    [KgAi,Sby,Eby] = lqr(AgAi,BgAi,QgAi,RgAi)
    
    %AvaDgAi = [-4,-4,-4,-4]';
    %KgAi = acker(AgAi,BgAi,AvaDgAi);
    %KgAi(4) = 600;
    Lg = acker(Ag',Cg',AvaDgAi(1:3)*20)';

    %Lgdisp(:,i) = Lg;
    %KaiNormPFAI(i) = norm(Kai(1:2))
    %REFfreq = 0.02 + i*0.0036;
    %REFfreqDisp(i) = REFfreq
    
    sim('ModeloNoLinealAlumnos',t);
    
    Xtot(:,i) = out(:,7);
    Ttot(:,i) = tiempo;
    Xreftot(:,i) = out(:,4);
    
    %ErrorPFAI(i) = sum(abs(Xref.Data(:) - Xtot(i).Data(:,1))/length(Xref.Data(:)))
    
    rehash
    i
end
kas = cell(2,1)
kas{1} = 'Kp,Kd,Km,Ki (cabeceo y alabeo): '
kas{2} = 'Kp,Kd,Km,Ki (guiñada): '
KcaAiT(2,3,5,:) = KgAiT(2,:,:,:)

%%
colormap jet
for k=2;
for j=1:5;
for i=1:Nloop;
    
  delete(h1);
  delete(h2);
  delete(h3);
  delete(h4);
  figure(3);
  
subplot(221);h1=plot(tiempo,(360/(2*pi))*out(:,2),'k',tiempo,(360/(2*pi))*permute(outaT(k,j,i,:),[4,2,3,1]),'-g','LineWidth',1.5);
title({'Margen Estabilidad (cabeceo y alabeo):' num2str(Minca(k,j,i))},'FontSize', 12);
xlabel('Tiempo(s)');ylabel('Alabeo(°)');
subplot(222);h2=plot(tiempo,(360/(2*pi))*out(:,3),'k',tiempo,(360/(2*pi))*permute(outcT(k,j,i,:),[4,2,3,1]),'-g','LineWidth',1.5);
ylabel('Cabeceo(°)');xlabel('Tiempo(s)');title({kas{k} KcaAiT(k,j,i,:)},'FontSize', 7);
subplot(223);h3=plot(tiempo,(360/(2*pi))*out(:,4),'k',tiempo,(360/(2*pi))*permute(outgT(k,j,i,:),[4,2,3,1]),'-g','LineWidth',1.5);
title({'Margen Estabilidad (guiñada):' num2str(Ming(k,j,i))},'FontSize', 12);
xlabel('Tiempo(s)');ylabel('Guiñada(°)');
subplot(224);h4=plot(tiempo,permute(outvT(k,j,i,:,1),[4,5,1,2,3]),'-b',tiempo,permute(outvT(k,j,i,:,2),[4,5,1,2,3]),'-m',tiempo,permute(outvT(k,j,i,:,3),[4,5,1,2,3]),'-r',tiempo,permute(outvT(k,j,i,:,4),[4,5,1,2,3]),'-y',tiempo,12*ones(1,length(tiempo)),'--k','LineWidth',1.5);
legend('Proa','Popa','Estribor','Babor','Saturación');xlabel('Tiempo(s)');title('Control motores');ylabel('Voltaje(V)')


  %title({'Freq_{Xr}(Hz)=' num2str(REFfreqDisp(i))},'FontSize', 9);
  %axis([0,20,-2,2])
  %legend('Referencia','Posición','Velocidad');
  %ylabel('Amplitud')
  %xlabel('Tiempo')
  
  drawnow;
  pause(0.01);
  
  %mov(k,j,i) = getframe(gcf)
  i
  j
end
end
end
%%
movie2avi(mov5, 'StabQuad.avi','fps',8);  
mov=mov1

ind=1
for k=1:2;
for j=1:5;
for i=1:Nloop;
mov5(ind)=mov(k,j,i)    
ind=ind+1;    
end
end
end